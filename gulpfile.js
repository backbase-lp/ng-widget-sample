'use strict';

var gulp = require('gulp');
var path = require('path');
var g = require('gulp-load-plugins')();
var del = require('del');
var runSequence = require('run-sequence');
var packageJson = require('./package.json');
var server = require('browser-sync');

var paths = {
	components : './components',
	scripts : './scripts',
	docs : './docs',
	target : './dist',
	templates : ['./templates', './index.html'],
	styles : './styles',
	test : './test',
};

/*----------------------------------------------------------------*/
/* Test/Lint/Verify
/*----------------------------------------------------------------*/
gulp.task('lint', function() {
	return gulp.src([
			paths.scripts + '/**/*.js',
			paths.components + '/**/*.js'
		])
		.pipe(g.eslint())
		.pipe(g.eslint.format())
		.pipe(g.eslint.failOnError())
		.on('error', g.notify.onError())
});

// gulp.task('test', function() {

// });


/*----------------------------------------------------------------*/
/* Scripts
/*----------------------------------------------------------------*/
// Compile
gulp.task('compile:scripts', ['lint'], function(done) {
	var config = {
		output: {
			filename: 'index.js',//path.join(packageJson.name,packageJson.main),
			libraryTarget: 'amd'
		},
		externals: [
			// Every non-relative module is external
			/^[a-z\-0-9]+$/,
			// Every module prefixed with "lp/" becomes external
			function(context, request, callback) {
				// "lp/<module>"
				if(/^lp/.test(request)) {
					return callback(null, request);
				}
				callback();
			}
		]
	};
	return gulp.src( packageJson.main )
		//.pipe(g.sourcemaps.init())
		.pipe(g.webpack(config))
		.on('error', g.notify.onError())
		.pipe(g.ngAnnotate())
		//.pipe(g.sourcemaps.write('./', { addComment: false }))
		.pipe(gulp.dest(paths.target + '/scripts'))
		.on('end', server.reload);
});

// Build
gulp.task('build:scripts', ['compile:scripts'], function(done) {
	return gulp.src( paths.target + '/scripts/index.js' )
		.pipe(g.uglify({
			mangle: { except: ['require'] }
		}))
		.pipe(g.rename('index.min.js'))
		.pipe(gulp.dest(paths.target + '/scripts'));
});

/*----------------------------------------------------------------*/
/* Styles
/*----------------------------------------------------------------*/
// Compile
gulp.task('compile:styles', [], function(done) {
	return gulp.src( paths.styles + '/base.less' )
		//.pipe(g.sourcemaps.init())
		// Use less compiler
		.pipe(g.less()).on('error', g.notify.onError())
		// Use sass compiler
		// .pipe(g.sass()).on('error', g.notify.onError())
		.pipe(server.reload({ stream:true }))
		//.pipe(g.sourcemaps.write('./', { addComment: false }))
		.pipe(gulp.dest(paths.styles));

});
// Build
gulp.task('build:styles', ['compile:styles'], function() {

	return gulp.src( paths.styles + '/base.css' )
		.pipe(g.less({compress: true}))
		.pipe(gulp.dest(paths.target + '/styles'))

});
/*----------------------------------------------------------------*/
/* Html
/*----------------------------------------------------------------*/
gulp.task('compile:html', [], function() {

});
gulp.task('build:html', [], function() {

});

/*----------------------------------------------------------------*/
/* Docs
/*----------------------------------------------------------------*/
// gulp.task('clean:docs', function (done) {
// 	del([paths.docs], done);
// });

// gulp.task('docs', ['clean:docs'], function() {
// 	return gulp.src(paths.scripts + '/**/*.js')
// 		.pipe(g.jsdoc(paths.docs));
// });

/*----------------------------------------------------------------*/
/* Build
/*----------------------------------------------------------------*/
gulp.task('clean:target', function (done) {
	del([paths.target], done);
});

gulp.task('build', function(done) {
	runSequence('clean:target', ['build:scripts', 'build:styles'], 'build:html',  done);
});


/*----------------------------------------------------------------*/
/* Package
/*----------------------------------------------------------------*/
// gulp.task('bump', function(){
// 	var bump = require('gulp-bump');
// 	var opts = {
// 		type:  g.util.env.type || 'patch',
// 		version: g.util.env.version || ''
// 	};
// 	return gulp.src(['./bower.json', './package.json'])
// 		.pipe(bump(opts))
// 		.pipe(gulp.dest('./'));

// });

/*----------------------------------------------------------------*/
/* Run (server/watcher)
/*----------------------------------------------------------------*/
gulp.task('server', function() {
	server({
		open: false,
		server: {
			baseDir: ['./', paths.target],
			https: true,
			minify: true,
			// port: 3000,
			//proxy: 'localhost:7777',
			routes: {

			}
		}
	});
});
gulp.task('server:reload', function () {
  server.reload();
});
// Watcher
gulp.task('start',['server'], function () {

	// watch scripts
	gulp.watch([
		paths.scripts + '/**/*.js',
		paths.components + '/**/*.js'
	], ['compile:scripts']);

	gulp.watch([
		paths.styles + '/**/*.less',
		paths.components + '/**/*.less'
	], ['compile:styles']);

	gulp.watch( paths.templates , ['server:reload']);

});

// Default task
gulp.task('default', [], function () {

});

module.exports = gulp;
