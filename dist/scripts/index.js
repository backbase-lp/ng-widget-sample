define(["lp/main","lp/modules/core"], function(__WEBPACK_EXTERNAL_MODULE_1__, __WEBPACK_EXTERNAL_MODULE_2__) { return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;/* WEBPACK VAR INJECTION */(function(module) {/**
	 * @module ng-widget-sample
	 * @version 1.0.0
	 * @file ng-widget-sample description
	 * @copyright Backbase Amsterdam
	 * @requires module:lp/main
	 * @requires module:lp/modules/core
	 * @requires interact
	 *
	 * @example Require Widget
	 * // add this in the index.html
	 * window.requireWidget( __WIDGET__ ,'scripts/index');
	 */

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function (require, exports, module) {

		'use strict';

		module.name = 'ng-widget-sample';

		/**
		 * Dependencies
		 */
		var main = __webpack_require__(1);
		var core = __webpack_require__(2);

		// Custom component
		var component = __webpack_require__(5);

		var deps = [
			core.name,
			component.name
		];

		// @ngInject
		function run(widget, utils) {
			// console.log(utils);
		}
		run.$inject = ["widget", "utils"];

		module.exports = main.createWidget(module.name , deps)
			.controller( __webpack_require__(3) )
			.config( __webpack_require__(4) )
			.run( run );
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(6)(module)))

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_1__;

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;/**
	 * Controllers
	 * @module controllers
	 */
	!(__WEBPACK_AMD_DEFINE_RESULT__ = function(require, exports) {

		'use strict';

		/**
		 * MainCtrl description.
		 */
		// @ngInject
		exports.MainCtrl = function() {
			var ctrl = this;
			ctrl.hello = 'Hello from MainCtrl';
		};
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;/**
	 * Config
	 * @module config
	 */
	!(__WEBPACK_AMD_DEFINE_RESULT__ = function (require, exports, module) {

	    'use strict';

	    // @ngInject
	    module.exports = function (widget, utils) {

	    };
	    module.exports.$inject = ["widget", "utils"];
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;/* WEBPACK VAR INJECTION */(function(module) {!(__WEBPACK_AMD_DEFINE_RESULT__ = function (require, exports, module) {

	    'use strict';

	    module.name = 'ng-component-sample';

	    var main = __webpack_require__(1);
	    var core = __webpack_require__(2);

	    var deps = [
	        core.name
	    ];

	    module.exports = main.createComponent(module.name , deps)
	        .directive(__webpack_require__(7));
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(6)(module)))

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = function(module) {
		if(!module.webpackPolyfill) {
			module.deprecate = function() {};
			module.paths = [];
			// module.parent = undefined by default
			module.children = [];
			module.webpackPolyfill = 1;
		}
		return module;
	}


/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_RESULT__ = function (require, exports) {
	    'use strict';
	    // @ngInject
	    exports.lpComponentSample = function(utils) {

	        function linkFn( scope, el, attrs, ngModelCtrl ) {

	        }

	        function compileFn() {
	            return linkFn;
	        }

	        return {
	            scope: {
	                config: '=lpComponentSample',
	                data: '=ngModel'
	            },
	            restrict: 'AEC',
	            require: '?^ngModel',
	            compile: compileFn,
	            template: [
	                '<ul class="ng-component-sample"> <h3>Custom component</h3>',
	                    '<li>configuration: {{config}}; </li>',
	                    '<li>data: {{data}}</li>',
	                '</ul>'
	                ].join('')
	        };
	    };
	    exports.lpComponentSample.$inject = ["utils"];
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }
/******/ ])});