# NG-WIDGET
Sample  Backbase Launchpad widget based on angularjs

## HOWTO
- Develop standalone
- Develop within a bundle
- Configure
- Run
- Controllers
- Custom Components
- Use Resources
- Unit Test it
- Write E2E support files
- Write Docs
- Style it
- Use templates
- Build the widget
- Version
- CHANGELOG.md
- Commit
- Integrate with current SVN trunk TBA


## Develop standalone
 1. download ng-widget-sample or generate it using bbscaf
 2. install devDependencies && resources
        npm i && bower i
 4. start the local server
        npm start
