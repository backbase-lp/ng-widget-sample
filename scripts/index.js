/**
 * @module ng-widget-sample
 * @version 1.0.0
 * @file ng-widget-sample description
 * @copyright Backbase Amsterdam
 * @requires module:lp/main
 * @requires module:lp/modules/core
 * @requires interact
 *
 * @example Require Widget
 * // add this in the index.html
 * window.requireWidget( __WIDGET__ ,'scripts/index');
 */

define( function (require, exports, module) {

	'use strict';

	module.name = 'ng-widget-sample';

	/**
	 * Dependencies
	 */
	var main = require('lp/main');
	var core = require('lp/modules/core');

	// Custom component
	var component = require('../components/lp-component-sample/scripts/index');

	var deps = [
		core.name,
		component.name
	];

	// @ngInject
	function run(widget, utils) {
		// console.log(utils);
	}

	module.exports = main.createWidget(module.name , deps)
		.controller( require('./controllers') )
		.config( require('./config') )
		.run( run );
});
