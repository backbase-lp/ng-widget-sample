/**
 * Controllers
 * @module controllers
 */
define(function(require, exports) {

	'use strict';

	/**
	 * MainCtrl description.
	 */
	// @ngInject
	exports.MainCtrl = function() {
		var ctrl = this;
		ctrl.hello = 'Hello from MainCtrl';
	};
});
