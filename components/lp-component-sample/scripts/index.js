define( function (require, exports, module) {

    'use strict';

    module.name = 'ng-component-sample';

    var main = require('lp/main');
    var core = require('lp/modules/core');

    var deps = [
        core.name
    ];

    module.exports = main.createComponent(module.name , deps)
        .directive(require('./lp-component-sample'));
});
