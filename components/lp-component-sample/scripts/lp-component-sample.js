define(function (require, exports) {
    'use strict';
    // @ngInject
    exports.lpComponentSample = function(utils) {

        function linkFn( scope, el, attrs, ngModelCtrl ) {

        }

        function compileFn() {
            return linkFn;
        }

        return {
            scope: {
                config: '=lpComponentSample',
                data: '=ngModel'
            },
            restrict: 'AEC',
            require: '?^ngModel',
            compile: compileFn,
            template: [
                '<ul class="ng-component-sample"> <h3>Custom component</h3>',
                    '<li>configuration: {{config}}; </li>',
                    '<li>data: {{data}}</li>',
                '</ul>'
                ].join('')
        };
    };
});
